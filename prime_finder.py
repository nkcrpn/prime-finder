import sys
import math

def is_int(num):
    try:
        int(num)
        return True
    except:
        return False

if len(sys.argv) < 2 or not is_int(sys.argv[1]):
    print "This program requires an input of a single integer."
    exit()

def remove_multiples(target, num_ls):
    for num in num_ls[num_ls.index(target)+1:]:
        if num % target == 0:
            num_ls.remove(num)

n = int(sys.argv[1])

nums_in_range = range(2, n+1)

for i in range(len(nums_in_range)):
    remove_multiples(nums_in_range[i], nums_in_range)
    if nums_in_range[i] >= math.sqrt(n):
        break

print nums_in_range
print "\nThere are %d number of primes between 0 and %d." %(len(nums_in_range),
                                                            n)
